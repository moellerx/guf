# Guf

Simple GIF-encoder library written in Javascript.
The library is written with help from [this guide](http://giflib.sourceforge.net/whatsinagif/index.html).
### Installation
Add the following to your dependencies in package.json and run `npm install`:
```json
"guf": "gitlab:moellerx/guf"
```

### Usage

Guf can create GIF files from a list of image buffers/path to image. If the list contains more than one image, the GIF will be animated. You can specify the animation speed though options (see next section).
```javascript
const guf = require('guf');
const fs = require('fs');
const anImageBuffer = fs.readFileSync('./some_image.png');
guf([
	'./path_to_some_image.jpg',
	'./path_to_some_other_image_png',
	anImageBuffer
])
	.then(buffer => {
		// Do something with GIF buffer
	});
```
### Options

The following options can be used with the library:
```javascript
/**
 * @{number} [msBetweenFrames] - Amount of milli seconds between each frame
 * @{boolean} [useAsync] - Split encoding into multiple worker processes VS. synchronous encoding
 * @{number} [maxThreads] - Used with async encoding. Specifies the maximum number of child processes
 * @{boolean} [applyDithering] - Whether or not to apply Floyd-Steinberg dithering during the encoding process
 * @{byte} [disposalMethod] - Choose between FULL_RESTORE, CLEAR_BACKGROUND and NONE
 */
const defaultOptions = {
 msBetweenFrames: 0,
 useAsync: true,
 maxThreads: 3,
 disposalMethod: 0x00, // NONE
 applyDithering: true
};
```
Example usage:
```javascript
const gufOptions = {
	useAsync: false,
	msBetweenFrames: 20
};
guf([
	'./img1.png',
	'./img2.png',
	'./img3.png'
], gufOptions)
	.then(buffer => /* ... */);
```
### Dependencies
* Jimp - for reading images into bitmaps
### License
MIT