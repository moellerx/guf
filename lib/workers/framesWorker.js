const OutputStream = require('../outputStream');
const {
    getGraphicsControlExtension,
    getImageDescriptor,
    getImageData
} = require('../gifBlocks');

// Wait for args to passed to the worker
process.on('message', args => {
    const outs = new OutputStream();
    const {
        image,
        height,
        width,
        disposalMethod,
        msBetweenFrames,
        amountOfColors
    } = args;

    outs.writeBytes(getGraphicsControlExtension(disposalMethod, msBetweenFrames));
    outs.writeBytes(getImageDescriptor(width, height));
    outs.writeBytes(getImageData(amountOfColors, image));

    // Send bytes to parent process
    process.send(outs.getBytes());
});