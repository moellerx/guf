/**
 * Applies LZW compression (GIF version w/ clear code and EOI) to a stream of color indexes
 * @param {number} amountOfColors - The amount of colors in the used color table
 * @param {number[]} colorIndexes - The indexes to compress
 */
function applyLZWCompression (amountOfColors, colorIndexes) {
    // GIF specific compression codes
    const clearCode = amountOfColors;
    const EOI = amountOfColors + 1;

    // Next available code
    let nextHighestCode = EOI + 1;

    // Prepare code stream
    const codeStream = [];
    const write = dec => codeStream.push(dec);

    const initialzeCodeTable = () => {
        const newCodeTable = {};
        for (let i = 0; i < amountOfColors; i++) {
            newCodeTable[i] = i;
        }
        newCodeTable[clearCode] = clearCode;
        newCodeTable[EOI] = EOI;

        nextHighestCode = EOI + 1;

        write(newCodeTable[clearCode]);

        return newCodeTable;
    };

    // Create new code table
    let codeTable = initialzeCodeTable();
    
    let indexBuffer = colorIndexes[0];
    let currentIndex;
    for (let i = 1; i < colorIndexes.length; i++) {
        currentIndex = colorIndexes[i];
        const lookupKey = `${indexBuffer},${currentIndex}`;
        if (codeTable[lookupKey] !== undefined) {
            indexBuffer += `,${currentIndex}`;
        } else {
            write(codeTable[indexBuffer]);

            // Re-init code table nextHighestCode gets bigger than 12 bits
            if (nextHighestCode > 0xFFF) {
                codeTable = initialzeCodeTable();
                codeTable[currentIndex] = colorIndexes[i];
            } else {
                codeTable[lookupKey] = nextHighestCode++;
            }
            
            indexBuffer = currentIndex;
        }
    }
    write(codeTable[indexBuffer]);
    write(codeTable[EOI]);
    return codeStream;
}

module.exports = applyLZWCompression;