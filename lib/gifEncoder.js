const globalColorTable = require('./globalColorTable');
const OutputStream = require('./outputStream');
const Worker = require('./worker');
const {
    readImage,
    ensureSameDimensions,
    createImageMatrices
} = require('./imageReading');
const {
    getHeader,
    getLogicalScreenDescriptor,
    getNetscapeExtension,
    getGraphicsControlExtension,
    getImageDescriptor,
    getImageData,
    getTrailer
} = require('./gifBlocks');

const DISPOSAL_METHODS = {
    FULL_RESTORE: 0x0C,
    CLEAR_BACKGROUND: 0x08,
    NONE: 0x00
};

/**
 * @typedef GifOptions
 * @type {object}
 * @property {number} [msBetweenFrames=0] - Amount of millis between each frame
 * @property {boolean} [useAsync=true] - Whether or not to split the heavier computations when encoding GIFS into child processes
 * @property {number} [maxThreads=3] - Used with async encoding. Specifies the maximum number of child processes
 * @property {boolean} [applyDithering=true] - Whether or not to apply Floyd-Steinberg dithering to the color matrix before encoding
 * @property {byte} [disposalMethod=-1] - Disposal method when encoding animated GIFS as a byte.
 * The following bits should be modified in the byte to represent a 3 bit disposal code: 000MMM00.
 * Defaults to NONE (0x00) if only one image is encoded, and RESTORE (0x0C) otherwise
 */
const defaultOptions = {
    msBetweenFrames: 0,
    useAsync: true,
    maxThreads: 3,
    disposalMethod: -1,
    applyDithering: true
};

/**
 * Merges user options with default options
 * @param {GifOptions} userOptions
 * @returns {GifOptions} the final options
 */
function getFinalOptions (userOptions) {
    const finalOptions = {};
    for (let [key, value] of Object.entries(defaultOptions)) {
        const userOpt = userOptions[key];
        if (userOpt !== undefined) {
            finalOptions[key] = userOpt;
        } else {
            finalOptions[key] = value;
        }
    }
    return finalOptions;
}

/**
 * Writes the image blocks to the GIF synchronously.
 * WARNING: Can take a long time. Consider using the asynchronous alternative if the image is large
 * @param {OutputStream} outs - The output stream of the GIF
 * @param {[[number[]]]} imgs - A list of matrixes containing indexes to the used color table
 * @param {number} dm - Disposal method
 * @param {number} msf - Millis between frames
 * @param {number} w - Width in pixels
 * @param {number} h - Height in pixels
 * @param {number} aoc - Amount of colors in the color table
 */
function writeFramesSync (outs, imgs, dm, msf, w, h, aoc) {
    for (let i = 0; i < imgs.length; i++) {
        const curImg = imgs[i];
        outs.writeBytes(getGraphicsControlExtension(dm, msf));
        outs.writeBytes(getImageDescriptor(w, h));
        outs.writeBytes(getImageData(aoc, curImg));
    }
}

/**
 * Writes the image blocks to the GIF asynchronously.
 * Does so by starting child processes to perform the encoding.
 * @param {OutputStream} outs - The output stream of the GIF
 * @param {[[number[]]]} imgs - A list of matrixes containing color indexes to represent images
 * @param {number} dm - Disposal method
 * @param {number} msf - Millis between frames
 * @param {number} w - Width in pixels
 * @param {number} h - Height in pixels
 * @param {number} aoc - Amount of colors in the color table
 * @param {number} mt - Maximum amount of jobs that can run simultaneously
 */
async function writeFramesAsync (outs, imgs, dm, msf, w, h, aoc, mt) {
    const worker = new Worker(mt);
    const pathToWorker = `${__dirname}\\workers\\framesWorker.js`;

    // Construct the jobs for the worker
    const jobs = imgs.map(img => {
        return {
            path: pathToWorker,
            args: {
                image: img,
                width: w,
                height: h,
                disposalMethod: dm,
                msBetweenFrames: msf,
                amountOfColors: aoc
            }
        };
    });

    // Perform the jobs and append the results to the GIF output stream
    const results = await worker.performJobs(jobs);
    for (let bytes of results) {
        outs.writeBytes(bytes);
    }
}

/**
 * Generates a GIF buffer
 * @param {number} width 
 * @param {number} height 
 * @param {[number[]]} images 
 * @param {GifOptions} options 
 */
async function generateGif (width, height, images, options) {
    const hasAnimation = images.length > 1;
    const amountOfColors = globalColorTable.length / 3;

    // Setup disposal method
    let disposalMethod;
    if (options.disposalMethod !== -1) {
        disposalMethod = options.disposalMethod;
    } else {
        // Use the 'restore' disposal method if the GIF has animation
        disposalMethod = hasAnimation ? DISPOSAL_METHODS.FULL_RESTORE : DISPOSAL_METHODS.NONE;
    }

    // Create the output stream for the GIF
    const outs = new OutputStream();

    // The following blocks should always be written into the GIF-file
    outs.writeBytes(getHeader());
    outs.writeBytes(getLogicalScreenDescriptor(width, height));
    outs.writeBytes(globalColorTable);

    // If the GIF has animation, add the Netscape extension
    if (hasAnimation) {
        outs.writeBytes(getNetscapeExtension());
    }

    // Add frames
    if (options.useAsync) {
        await writeFramesAsync(
            outs,
            images,
            disposalMethod,
            options.msBetweenFrames,
            width,
            height,
            amountOfColors,
            options.maxThreads
        );
    } else {
        writeFramesSync(
            outs,
            images,
            disposalMethod,
            options.msBetweenFrames,
            width,
            height,
            amountOfColors
        );
    }

    // End with trailer
    outs.writeBytes(getTrailer());

    return Buffer.from(outs.getBytes());
}

/**
 * Creates a GIF from a list of images. Input images can be either buffers or paths to image files.
 * All the images must have the same dimensions (width, height).
 * @param {(string|Buffer)[]} images - The images that should make up the GIF
 * @param {GifOptions} [options] - Options for GIF encoding
 * @returns {Promise<Buffer>} the encoded GIF as a buffer
 */
async function createGif (images, options = {}) {
    // Merge user options with default options
    const finalOptions = getFinalOptions(options);

    // Read the image data that the GIF should contain
    const bitmaps = await Promise.all(images.map(image => readImage(image)));

    // Ensure that all input images have the same dimensions
    const { height, width } = ensureSameDimensions(bitmaps);

    // Create the color index matrices that makes up the GIF
    const colorIndexMatrices = createImageMatrices(bitmaps, globalColorTable, width, height, finalOptions.applyDithering);

    // Write the GIF file into a buffer
    return await generateGif(width, height, colorIndexMatrices, finalOptions);
}

module.exports = createGif;