const applyLZWCompression = require('./lzw');
const packCodes = require('./codePacker');
const { flat } = require('./util');

/**
 * Gets the GIF89 header block
 */
function getHeader () {
    return [0x47, 0x49, 0x46, 0x38, 0x39, 0x61];
}

/**
 * Converts a decimal -> two bytes in the little-endian format
 * @param {number} num - The decimal to convert
 */
function twoByteLE (num) {
    return [num % 256, Math.floor(num / 256)];
}

/**
 * Get the Logical Screen Description block.
 * It describes that the GIF uses a global color table with a length of 0xFF
 * @param {number} width - The width of the image in pixels
 * @param {number} height - The height of the image in pixels
 */
function getLogicalScreenDescriptor (width, height) {
    return [...twoByteLE(width), ...twoByteLE(height), 0xF7, 0x0, 0x0];
}

/**
 * Gets the Netscape extension used for animation.
 * Bytes on index 16 & 17 describes that the animation should loop forever
 */
function getNetscapeExtension () {
    return [0x21, 0xFF, 0x0B, 0x4E, 0x45, 0x54, 0x53, 0x43, 0x41, 0x50, 0x45, 0x32, 0x2E, 0x30, 0x03, 0x01, 0x00, 0x00, 0x00];
}

/**
 * Gets a Graphics control extension.
 * Byte on index 3 is a packed byte, which only describes the disposal method (for now)
 * @param {number} disposalMethod - The disposal method to use
 * @param {number} msBetweenFrames - Millis between frame changes
 */
function getGraphicsControlExtension (disposalMethod, msBetweenFrames = 0) {
    const convertedToMs = twoByteLE(msBetweenFrames / 10);
    return [0x21, 0xF9, 0x04, disposalMethod, ...convertedToMs, 0x0, 0x0];
}

/**
 * Gets an Image description.
 * Each block of image data starts with an image descriptor
 * @param {number} width - The width of the image in pixels
 * @param {number} height - The height of the image in pixels
 */
function getImageDescriptor (width, height) {
    return [0x2C, 0x0, 0x0, 0x0, 0x0, ...twoByteLE(width), ...twoByteLE(height), 0x0];
}

/**
 * Creates the actual Image data blocks, packs them, and fragments them into sub blocks
 * @param {number} amountOfColors - Length of the color table
 * @param {[number[]]} colorIndexes - The image represented as a matrix of indexes to colors in the color table 
 */
function getImageData (amountOfColors, colorIndexes) {
    // Compress the color indexes
    const compressedIndexes = applyLZWCompression(amountOfColors, flat(colorIndexes));
    
    // Pack the code stream
    return packCodes(compressedIndexes, amountOfColors);
}

/**
 * Gets the trailer that should appear in the end of every GIF
 */
function getTrailer () {
    return [0x3B];
}

module.exports = {
    getHeader,
    twoByteLE,
    getLogicalScreenDescriptor,
    getNetscapeExtension,
    getGraphicsControlExtension,
    getImageDescriptor,
    getImageData,
    getTrailer
};