const { fork } = require('child_process');

/**
 * Used by the Worker class
 * @typedef WorkerJob
 * @type {object}
 * @property {string} path - The path to the worker file
 * @property {*} args - The args for the worker
 */

/**
 * Allows for splitting work up into multiple child processes
 */
class Worker {
    /**
     * Constructs a new worker object
     * @param {number} [maxThreads] - The maximum amount of concurrent workers
     */
    constructor (maxThreads = 3) {
        this.maxThreads = maxThreads;
    }

    /**
     * Performs the specified jobs in child processes
     * @param {WorkerJob[]} jobs 
     * @returns {Promise<any[]>} results of the jobs as a promise
     */
    performJobs (jobs) {
        let finishedJobs = 0;
        let currentWorkers = 0;
        let nextJob = 0;
        let maxThreads = this.maxThreads;
        let jobResults = [];

        return new Promise((resolve, reject) => {
            // Start the first portion of jobs
            for (let i = 0; i < maxThreads; i++) performJob();

            function performJob () {
                // Ensure worker amount does not exceed maximum
                // Do nothing when there are no more jobs
                if (currentWorkers >= maxThreads) return;
                if (nextJob === jobs.length) return;

                // Save job index for job callback
                const cji = nextJob;

                // Bump nextJob and currentWorkers
                const currentJob = jobs[nextJob++];
                currentWorkers++;
                
                // Start the job in another process
                const worker = fork(currentJob.path);

                // Send the arguments for the job
                worker.send(currentJob.args);

                // Handle result and error
                worker.on('message', result => jobsDone(worker, cji, result));
                worker.on('error', reject);
            }

            function jobsDone (worker, jobIndex, result) {
                finishedJobs++;
                currentWorkers--;
                jobResults[jobIndex] = result;
                worker.kill();

                // When all jobs have been run, return the results
                if (finishedJobs === jobs.length) {
                    resolve(jobResults);
                } else {
                    performJob();
                }
            }
        });
    }
}

module.exports = Worker;