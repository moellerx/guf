const jimp = require('jimp');
const Worker = require('./worker');
const { supressError } = require('./util');

/**
 * Finds a color in the color table that resembles the input RGB values the most
 * @param {number[]} colorTable - List of RGB values. One Color is represented pr. 3 indexes.
 * @param {number} r - The amount of red
 * @param {number} g - The amount of green
 * @param {number} b - The amount of blue
 */
function findClosestColorIndex (colorTable, r, g, b) {
    const { abs } = Math;
    let closestIndex = 0;
    let closestIndexDiffSum = Infinity;
    for (let i = 0; i < colorTable.length; i += 3) {
        const rDiff = abs(r - colorTable[i]),
              gDiff = abs(g - colorTable[i + 1]),
              bDiff = abs(b - colorTable[i + 2]);

        const diffSum = rDiff + gDiff + bDiff;

        if (diffSum < closestIndexDiffSum) {
            closestIndex = i / 3;
            closestIndexDiffSum = diffSum;
        }
    }
    return closestIndex;
}
/**
 * Gets the RGB value from the color table at the specified index
 * @param {number[]} colorTable - The color table to read from
 * @param {number} index - The index to grab the RGB value from
 */
function lookupRgb (colorTable, index) {
    const correctedIndex = index * 3;
    return {
        r: colorTable[correctedIndex],
        g: colorTable[correctedIndex + 1],
        b: colorTable[correctedIndex + 2]
    };
}

/**
 * Converts RGB to hex
 * @param {number} r - Red
 * @param {number} g - Green
 * @param {number} b - Blue
 */
function rgb2hex (r, g, b) {
    return jimp.rgbaToInt(r, g, b, 0xFF);
}

/**
 * Applies Floyd-Steinberg dithering at (x, y) in the specified image.
 * Assumes that the outer loop goes from top to bottom, and the inner loop
 * goes from left to right.
 * Modifies the input image.
 * @param {*} img - The image to apply dithering to
 * @param {number} x - The x-coordinate in the image
 * @param {number} y - The y-coordinate in the image
 * @param {number} qeR - Quant error for red
 * @param {number} qeG - Quant error for green
 * @param {number} qeB - Quant error for blue
 */
function applyFloydSteinbergDithering (img, x, y, qeR, qeG, qeB) {
    // Supress errors when the algorith goes out of bounds
    supressError(() => {
        const pR = jimp.intToRGBA(img.getPixelColor(x + 1, y));
        img.setPixelColor(rgb2hex(pR.r + qeR * 7 / 16, pR.g + qeG * 7 / 16, pR.b + qeB * 7 / 16), x + 1, y);
    });
        
    supressError(() => {
        const pLD = jimp.intToRGBA(img.getPixelColor(x - 1, y + 1));
        img.setPixelColor(rgb2hex(pLD.r + qeR * 3 / 16, pLD.g + qeG * 3 / 16, pLD.b + qeB * 3 / 16), x - 1, y + 1);
    });

    supressError(() => {
        const pD = jimp.intToRGBA(img.getPixelColor(x, y + 1));
        img.setPixelColor(rgb2hex(pD.r + qeR * 5 / 16, pD.g + qeG * 5 / 16, pD.b + qeB * 5 / 16), x, y + 1);
    });

    supressError(() => {
        const pRD = jimp.intToRGBA(img.getPixelColor(x + 1, y + 1));
        img.setPixelColor(rgb2hex(pRD.r + qeR * 1 / 16, pRD.g + qeG * 1 / 16, pRD.b + qeB * 1 / 16), x + 1, y + 1);
    });
}

/**
 * Creates a bitmap from a path to an image or a bufffer
 * @param {(string|Buffer)} image - The image to read into memory
 */
function readImage (image) {
    return new Promise(async (resolve, reject) => {
        if (image.bitmap) {
            resolve(await createImageFromRGBMatrix(image.bitmap));
        } else {
            jimp.read(image, (err, bitmap) => {
                if (err) reject(err);
                resolve(bitmap);
            });
        }
    });
}

/**
 * Converts the input bitmap to a format that can be interpreted by the GIF encoder
 * @param {[number[]]} bitmap - The bitmap to convert
 */
function createImageFromRGBMatrix (bitmap) {
    const width = bitmap[0].length / 3;
    const height = bitmap.length;

    return new Promise((resolve, reject) => {
        new jimp(width, height, (err, image) => {
            if (err) reject(err);

            for (let y = 0; y < bitmap.length; y++) {
                for (let x = 0; x < bitmap[0].length / 3; x++) {
                    const i = x * 3;
                    const r = bitmap[y][i],
                          g = bitmap[y][i + 1],
                          b = bitmap[y][i + 2];
                    
                    image.setPixelColor(rgb2hex(r, g, b), x, y);
                }
            }
            resolve(image);
        });
    });
}

/**
 * Ensures that a list of images all have the same dimensions
 * @param {*[]} images - The images to perform dimension check on
 * @returns the width and height of the images
 */
function ensureSameDimensions (images) {
    const firstImage = images[0].bitmap;
    for (let i = 1; i < images.length; i++) {
        const currentImage = images[i].bitmap;
        if (currentImage.width !== firstImage.width) {
            throw new Error('The images should have the same width in pixels');
        }

        if (currentImage.height !== firstImage.height) {
            throw new Error('The images should have the same height in pixels');
        }
    }
    return {
        width: firstImage.width,
        height: firstImage.height
    };
}

/**
 * Creates a list of image matrices containing indexes to the specified color table
 * @param {*[]} images - A list of images with homogeneous dimensions
 * @param {number[]} colorTable - The color table to referenced
 * @param {number} width - The width of the images in pixels
 * @param {number} height - The height of the images in pixels
 * @param {boolean} useDithering - Whether or not to apply Floyd-Steinberg dithering to the image
 */
function createImageMatrices (images, colorTable, width, height, useDithering) {
    return images.map(img => createImageMatrix(colorTable, img, width, height, useDithering));
}

/**
 * Creates a matrix of indexes to the specified color table representing the RGB values of the input image
 * @param {number[]} colorTable - List of RGB values. One Color is represented pr. 3 indexes.
 * @param {*} img - The image to create the matrix from
 * @param {number} width - The width of the image in pixels
 * @param {number} height - The height of the image in pixels
 * @param {boolean} useDithering - Whether or not to apply Floyd-Steinberg dithering to the image
 */
function createImageMatrix (colorTable, img, width, height, useDithering) {
    const image = [];
    for (let y = 0; y < height; y++) {
        const row = [];
        for (let x = 0; x < width; x++) {
            const { r, g, b } = jimp.intToRGBA(img.getPixelColor(x, y));
            const closestIndex = findClosestColorIndex(colorTable, r, g, b);

            // Perform Floyd-Steinberg dithering (if specified)
            if (useDithering) {
                // Grab the RGB values from the closest pixel
                const closestPixel = lookupRgb(colorTable, closestIndex);

                // Quant errors for RGB
                const qeR = r - closestPixel.r;
                const qeG = g - closestPixel.g;
                const qeB = b - closestPixel.b;

                applyFloydSteinbergDithering(img, x, y, qeR, qeG, qeB);
            }
            
            row.push(closestIndex);
        }
        image.push(row);
    }
    return image;
}

module.exports = {
    readImage,
    ensureSameDimensions,
    createImageMatrix,
    createImageMatrices
};