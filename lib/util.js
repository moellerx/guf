/**
 * Flattens a matrix
 * @param {[any[]]} matrix 
 */
function flat (matrix) {
    let toReturn = [];
    matrix.forEach(row => toReturn = [...toReturn, ...row]);
    return toReturn;
}

/**
 * Gets the minimum amount of bits needed to represent the decimal
 * @param {number} decOrHex - The number to evaluate
 */
function minBitSize (decOrHex) {
    let currentMaxBitValue = 0;
    for (let i = 0; i < Infinity; i++) {
        currentMaxBitValue += Math.pow(2, i);
        if (currentMaxBitValue >= decOrHex) return i + 1;
    }
}

/**
 * Extracts a number of elements from an array starting from a specific point
 * @param {any[]} arr - The array to read from 
 * @param {number} from - The index to start reading from
 * @param {number} count - Amount of elements to read
 */
function getRange (arr, from, count) {
    const toReturn = [];
    for (let i = from; i < from + count; i++) {
        toReturn.push(arr[i]);
    }
    return toReturn;
}

/**
 * Converts a decimal to bits with a minimum bit length
 * @param {number} dec - The decimal to convert
 * @param {number} minBitLength - The minimum bit length 
 */
function convertDecToBits (dec, minBitLength) {
    const bits = [];
    let temp = dec;

    while (temp > 0) {
        if ((temp & 1) === 1) {
            bits.push(true);
        } else {
            bits.push(false);
        }
        temp >>= 1;
    }

    while (bits.length < minBitLength) {
        bits.push(false);
    }

    return bits;
}

/**
 * Converts a sequence of bits into a decimal
 * @param {boolean[]} bArr 
 */
function bitArray2dec (bArr) {
    let bitString = [];
    bArr.forEach(bit => bitString.push(bit ? '1' : '0'));
    return parseInt(bitString.join(''), 2);
}

/**
 * Runs a function that will be silent if it encounters an error
 * @param {*} funcToRun - The function which should have its errors supressed
 */
function supressError (funcToRun) {
    try {
        funcToRun();
    } catch (e) {}
}

module.exports = {
    flat, minBitSize, getRange, convertDecToBits, bitArray2dec, supressError
};