/**
 * Simulates an output byte stream
 */
class OutputStream {
    /**
     * Constructs an empty byte stream
     * @constructor
     */
    constructor () {
        this.bytes = [];
    }

    /**
     * Writes a list of bytes to the end of the byte stream
     * @param {number[]} bytes - The bytes to write
     */
    writeBytes (bytes) {
        for (let i = 0; i < bytes.length; i++) {
            this.bytes.push(bytes[i]);
        }
    }

    /**
     * Gets all the bytes from the byte stream
     */
    getBytes () {
        return this.bytes;
    }
}

module.exports = OutputStream;