const { flat, minBitSize, getRange, convertDecToBits, bitArray2dec } = require('./util');

/**
 * Packs the compressed code stream into bytes using flexible code sizes
 * @param {number[]} codeStream - The stream of compressed codes to pack
 * @param {number} amountOfColors - The length of the used color table 
 */
function packCodeStream (codeStream, amountOfColors) {
    const packedBytes = [];
    const bits = [];

    let currentCodeWidth = minBitSize(amountOfColors + 1);
    let codeCount = 0;
    let codeWidthIncreaseThreshold = Math.pow(2, currentCodeWidth) - 1;

    for (let code of codeStream) {
        if (codeCount + (amountOfColors - 1) >= codeWidthIncreaseThreshold) {
            if (currentCodeWidth < 12) currentCodeWidth++;
            codeWidthIncreaseThreshold = Math.pow(2, currentCodeWidth) - 1;
        }
    
        const codeBits = convertDecToBits(code, currentCodeWidth);
        for (let i = 0; i < codeBits.length; i++) {
            bits.push(codeBits[i]);
        }

        codeCount++;
        // amountOfColors = clear code
        if (code === amountOfColors) {
            currentCodeWidth = minBitSize(amountOfColors + 1);
            codeWidthIncreaseThreshold = Math.pow(2, currentCodeWidth) - 1;
            codeCount = 1;
        }
    }

    const zeroesNeeded = 8 - (bits.length % 8);
    for (let i = 0; i < zeroesNeeded; i++) bits.push(false);

    for (let i = 0; i < bits.length; i += 8) {
        let reversedByte = getRange(bits, i, 8);
        reversedByte = reversedByte.reverse();
        packedBytes.push(bitArray2dec(reversedByte));
    }

    return packedBytes;
}

/**
 * Fragments a stream of bytes into sub blocks
 * @param {number[]} packedBytes - The stream of bytes to fragment
 */
function fragmentPackedBytes (packedBytes) {
    const subBlocks = [];
    for (let i = 0; i < packedBytes.length; i++) {
        const currentByte = packedBytes[i];
        if (i % 255 === 0) {
            subBlocks.push([]);
        }
        subBlocks[subBlocks.length - 1].push(currentByte);
    }

    subBlocks.forEach(block => block.unshift(block.length));

    return flat(subBlocks);
}

/**
 * Packs & fragments a stream of codes
 * @param {number[]} codeStream - The stream of compressed codes
 * @param {number} amountOfColors - The length of the used color table 
 */
function pack (codeStream, amountOfColors) {
    // Pack the code stream
    const packedBytes = packCodeStream(codeStream, amountOfColors);

    // Segment code stream into sub blocks
    const subBlocks = fragmentPackedBytes(packedBytes);

    return [minBitSize(amountOfColors - 1), ...subBlocks, 0x00];
}

module.exports = pack;